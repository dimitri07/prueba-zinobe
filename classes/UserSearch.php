<?php
namespace Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class UserSearch extends Eloquent
{
   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

    protected $fillable = [
       'searchId', 'uid', 'needle', 'result', 'created_at', 'updated_at',  
    ];

   /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

   protected $hidden = [];

   /*
   * Get Todo of User
   *
   */

    public function todo()
    {
        return $this->hasMany('Todo');
    }
}