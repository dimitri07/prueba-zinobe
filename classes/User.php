<?php
namespace Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent
{
   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

    protected $fillable = [
       'name', 'document', 'email', 'password', 'country', 'created_at', 'updated_at',  
    ];

   /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */

   protected $hidden = [];

   /*
   * Get Todo of User
   *
   */

   public function todo()
   {
      return $this->hasMany('Todo');
   }
}