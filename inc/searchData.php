<?php
require_once 'config.php';
// setup for twig 
require_once '../vendor/autoload.php';
// setup for eloquent
require "../bootstrap.php";
// add here  all class models
require_once "../classes/User.php";
require_once "../classes/UserSearch.php";
use Models\User as User;
use Models\UserSearch as UserSearch;

$needle = $_POST['needle'];
$containerData = getExternalData(EXTERNAL_DATA_ONE);
$response = applySearch($containerData, $needle);
$containerData = getExternalData(EXTERNAL_DATA_TWO);
$response = array_merge($response, applySearch($containerData, $needle));

// save in search history
session_start();
$userSearch = new UserSearch();
$userSearch->uid = (int) $_SESSION['uid'];
$userSearch->needle = $needle;
$userSearch->result = json_encode($response); 
$userSearch->save();  

echo json_encode($response);




