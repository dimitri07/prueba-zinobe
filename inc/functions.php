<?php

function sendBasicOutput(int $code, $data, string $description) : array
{
    return [
        'code'          => $code,
        'data'          => $data,
        'description'   => $description
    ]; 
}

function getExternalData(string $urlExternalResource) :array 
{
    $handle = curl_init();
    curl_setopt($handle, CURLOPT_URL, $urlExternalResource);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($handle);
    curl_close($handle);
    $output = json_decode($output);
    //var_dump($output->objects);
    return $output->objects;
}

function applySearch(array $containerData, string $needle) :array
{
    $response = [];
    $needle = trim($needle);
    foreach ($containerData as $key => $user) 
    {
        if (isset($user->first_name) && isset($user->email)) 
        {
            if ($user->first_name === $needle || $user->email === $needle) 
            {
                //var_dump($user);
                $response[] = $user;    
            }    
        }
        elseif(isset($user->primer_nombre) && isset($user->correo)) 
        {
            if (($user->primer_nombre === $needle) || ($user->correo === $needle))
            {
                //var_dump($user);
                $response[] = $user;
            }
        }  
    }
    return $response;
}

