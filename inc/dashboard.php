<?php
require_once 'config.php';
// setup for twig 
require_once '../vendor/autoload.php';
// setup for eloquent
require "../bootstrap.php";
// add here  all class models
require_once "../classes/UserSearch.php";
use Models\UserSearch as UserSearch;

$loader = new \Twig\Loader\FilesystemLoader('../templates');
$twig = new \Twig\Environment($loader, [
    //'cache' => 'templates',
]);
// check session vars
session_start();
if ((!isset($_SESSION['document']))) 
{
    echo $twig->render('login.html', [
        'name' => ''
    ]);
    exit(); 
}
// get search history
$userSearch = (new UserSearch())
    ->where('uid', (int) $_SESSION['uid'])
    ->get();
foreach ($userSearch as $key => $search) 
{
   $search->result = json_decode($search->result);
}

echo $twig->render('dashboard.html', [
    'name'              => $_SESSION['name'],
    'email'             => $_SESSION['email'],
    'document'          => $_SESSION['document'],
    'historySeacrh'     => $userSearch
]);

