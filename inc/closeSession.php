<?php
require_once 'config.php';
session_start();
session_destroy();

echo json_encode(sendBasicOutput(200, [], 'Operación exitosa'));