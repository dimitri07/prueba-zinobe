<?php
require_once 'config.php';
require "../bootstrap.php";
require_once "../classes/User.php";
use Models\User as User;

$user = User::where('document', $_POST['document'])
    ->Where('password',  sha1($_POST['password']))
    ->get();

if ($user->isEmpty()) 
{
    echo json_encode(sendBasicOutput(102, [], 'Credenciales incorrectas'));   
}
else 
{
    // save data in sessions
    $user = $user->first();
    session_start();
    $_SESSION['name'] = $user->name;
    $_SESSION['document'] = $user->document;
    $_SESSION['email'] = $user->email;
    $_SESSION['uid'] = $user->uid;
    echo json_encode(sendBasicOutput(200, $user, 'Login correcto'));
}

