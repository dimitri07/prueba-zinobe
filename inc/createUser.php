<?php
require "config.php";
require "../bootstrap.php";
require_once "../classes/User.php";
use Models\User as User;

$document = $_POST['document'];
$email = $_POST['email'];
$user = User::where('document', $document)
    ->orWhere('email', $email)
    ->get();

if (!$user->isEmpty()) 
{
    echo json_encode(sendBasicOutput(102, [], 'El documento ó el email ya se ecnuentran regitsrados en el sistema'));
    exit();    
}

$user = User::Create([
    'name'      => $_POST['name'],
    'document'  => $document,
    'email'     => $email,
    'password'  => sha1($_POST['password']),
    'country'   => $_POST['country']
]);

echo json_encode(sendBasicOutput(200, [], 'Operación exitosa'));



