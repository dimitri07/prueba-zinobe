<?php
// General config
require_once 'inc/config.php';
// setup for twig 
require_once 'vendor/autoload.php';
// setup for eloquent
require "bootstrap.php";
// add here  all class models
require_once "classes/User.php";
require_once "classes/UserSearch.php";
use Models\UserSearch as UserSearch;

$loader = new \Twig\Loader\FilesystemLoader('templates');
$twig = new \Twig\Environment($loader, [
    //'cache' => 'templates',
]);

session_start();
if (isset($_SESSION['document'])) 
{   
    // get search history
    $userSearch = (new UserSearch())
        ->where('uid', (int) $_SESSION['uid'])
        ->get();
    foreach ($userSearch as $key => $search)    
    {
        $search->result = json_decode($search->result);
    }

    echo $twig->render('dashboard.html', [
    'name'              => $_SESSION['name'],
    'email'             => $_SESSION['email'],
    'document'          => $_SESSION['document'],
    'historySeacrh'     => $userSearch
    ]);
}
else
{
    echo $twig->render('register.html', []);
}


