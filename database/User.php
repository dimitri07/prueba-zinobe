<?php
require "../bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('users', function ($table) {
    $table->increments('uid');
    $table->string('name');
    $table->string('document')->unique();
    $table->string('email')->unique();
    $table->string('password');
    $table->string('country');
    $table->timestamps();
});