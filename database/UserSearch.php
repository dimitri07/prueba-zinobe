<?php
require "../bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('user_searches', function ($table) {
    $table->increments('searchId');
    $table->string('uid');
    $table->string('needle');
    $table->text('result');
    $table->timestamps();
});