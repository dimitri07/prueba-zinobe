# Directorio de Usuarios - Test Zinobe 

## Instalación y configuración de la aplicación
Una vez descargada la aplicación y ubicada en un servidor que soporte PHP, debe 
realizar las siguientes configuraciones

### Variables de configuración
1) Dirajase a la carpeta inc en el archivo config.php y modifique el valor de la constante BASE_URL
por el path del servidor donde va a poner en ejecución la aplicación
2) Repita el procedimiento anterior yendo esta vez a la carpeta templates, en el archivo skeleton.html, modificando el valor de la variable baseUrl por el mismo valor path donde va a poner a correr la ejecución

### Configuración BD 
1) En el archivo bootstrap.php ubicado en la raíz del proyecto, actualice las credenciales correctas de su bd
2) Para generar las tablas de la BD puede seguir una de las siguientes opciones: 
    #### Usar el script de BD que se encuentra en la carpeta scriptDB
    #### Correr las migraciones desde el navegador:
        Para ello en su navegador debe apuntar a la carpeta database y alli se encontrará con los archivos
        php que representan cada tabla, haciendo click sobre cada uno de ellos para ejecutarlos desde el navegador
        se generará la tabla en la BD

### Descripción tecnologías usadas
    PHP 7
    Motor de plantillas: Twig
    ORM: Eloquent
